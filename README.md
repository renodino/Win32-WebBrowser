# NAME

Win32::WebBrowser - open the default web browser on Win32

# SYNOPSIS

        use Win32::WebBrowser;
        
        open_browser('file://D:\\some\\directory\\myfile.html');

# DESCRIPTION

In a separate detached process, opens the default web browser instance for the specified URL.
The browser executable is determined by reading the registry.

# METHODS

Only a single exported method is provided:

## $result = open\_browser($url)

Create a separate detached process to run the default browser for the platform
(as specified in the registry) and supply the given `$url` to it.
Returns 1 on success, undef with the error message in `$@` on failure.

# SEE ALSO

[HTML::Display](https://metacpan.org/pod/HTML::Display) provides similar functionality for multiple OSes/platforms,
but does not currently (as of version 0.39) provide a working
Windows solution.

# AUTHOR, COPYRIGHT, and LICENSE

Copyright(C) 2007, Dean Arnold, Presicient Corp., USA. All rights reserved.

Permission is granted to use this software under the same terms as Perl itself.
Refer to the [Perl Artistic](https://metacpan.org/pod/perlartistic) license for details.
